import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { APIURL } from '../constants';
import { Errors } from '../models/errors.model';

@Injectable()
export class UtilsService {

  constructor(private http: HttpClient) {}

  async post<T>(url: string, body: any, options: any = {}): Promise<T | Errors> {
    const response = await this.http.post<T | Errors>(`${ APIURL }${ url }`, body, options).toPromise();
    if (!response || response.hasOwnProperty('errors')) {
      return Promise.reject((<any>response).errors);
    }
    return <any>response;
  }

  async get<T>(url: string, query: any, options: any = {}): Promise<T | Errors> {
    const params = Object.keys(query);
    const queryString = !params.length
      ? ''
      : Object.keys(query).reduce((accumulator, key) => {
        return `${ accumulator }${ accumulator === '?' ? '' : '&' }${ key }=${ query[key] }`;
      }, '?');
    const response = await this.http.get<T | Errors>(`${ APIURL }${ url }${ queryString }`, options).toPromise();
    if (!response || response.hasOwnProperty('errors')) {
      return Promise.reject((<any>response).errors);
    }
    return <any>response;
  }
}
