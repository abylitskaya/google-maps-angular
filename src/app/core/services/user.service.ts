import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';

import { User, UserMetaData } from '../models/user.model';
import { UtilsService } from './utils.service';
import { Errors } from '../models/errors.model';

@Injectable()
export class UserService extends UtilsService {
  authenticatedSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(undefined);
  private userId: string;
  private token: string;

  constructor(http: HttpClient) {
    super(http);

    const init = async () => {
      const storedUserId = localStorage.getItem('user_id');
      const storedToken = localStorage.getItem('token');
      if (storedUserId && storedToken) {
        this.userId = storedUserId;
        this.token = storedToken;
        let user;
        try {
          user = await this.getUser();
        } catch {
          this.signOut();
          return;
        }
        if (user) {
          this.authenticatedSubject.next(true);
          return;
        }
      }

      this.signOut();
    };
    init();
  }

  isAuthenticated(): Observable<boolean> {
    return this.authenticatedSubject.asObservable();
  }

  authenticate(data: UserMetaData): any {
    this.authenticatedSubject.next(true);
    this.userId = data._id;
    this.token = data.token;
    localStorage.setItem('user_id', data._id);
    localStorage.setItem('token', data.token);
  }

  checkLogin(user: User): Promise<UserMetaData | Errors> {
    return this.post('/users/login', { user })
      .then((dbUser: UserMetaData) => {
        if (dbUser) {
          this.authenticate(dbUser);
          return dbUser;
        }
      });
  }

  register(user: User): Promise<UserMetaData | Errors> {
    return this.post('/users/', { user })
      .then((dbUser: UserMetaData) => {
        if (dbUser) {
          this.authenticate(dbUser);
          return dbUser;
        }
      });
  }

  getUser(): Promise<User | Errors> {
    return this.get<User>('/users/current', { id: this.userId }, this.getHeaders());
  }

  signOut(): void {
    localStorage.removeItem('user_id');
    localStorage.removeItem('token');
    this.userId = '';
    this.token = '';
    this.authenticatedSubject.next(false);
  }

  private getHeaders(): any {
    return {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Token ${ this.token }`,
      }
    };
  }
}
