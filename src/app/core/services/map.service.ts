import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { State } from '../models/state.model';

@Injectable()
export class MapService {

  constructor(private http: HttpClient) {}

  getStates(): Promise<State[]> {
    return this.http.get<State[]>('./assets/data/states.json').toPromise();
  }
}
