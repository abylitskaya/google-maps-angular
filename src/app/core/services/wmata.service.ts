import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { BusStop, BusStopsParams } from '../models/bus-stop.model';
import { Route } from '../models/route.model';
import { BusStopSchedule } from '../models/bus-stop-schedule';

const API_URL = 'https://api.wmata.com/Bus.svc/json/';
const API_KEY = 'a9e8aad82e254fd49eba4b2604130dea';

@Injectable()
export class WMATAService {
  private headers: any;

  constructor(private http: HttpClient) {
    this.headers = {
      Accept: 'application/json',
      api_key: API_KEY,
    };
  }

  getBusStops(params?: BusStopsParams): Promise<BusStop[]> {
    const { centerLat, centerLng, radius } = new BusStopsParams(params);
    const query = `?Lat=${ centerLat }&Lon=${ centerLng }&Radius=${ radius * 1000 }`;
    return this.http
      .get<{ Stops: BusStop[] }>(`${ API_URL }jStops${ query }`, { headers: this.headers })
      .toPromise()
      .then(data => data.Stops);
  }

  getRoutes(): Promise<Route[]> {
    return this.http
      .get<{ Routes: Route[] }>(`${ API_URL }jRoutes`, { headers: this.headers })
      .toPromise()
      .then(data => data.Routes);
  }

  getBusStopSchedule(stopId: string): Promise<any[]> {
    const query = `?StopID=${ stopId }`;
    return this.http
      .get<BusStopSchedule>(`${ API_URL }jStopSchedule${ query }`, { headers: this.headers })
      .toPromise()
      .then((schedule) => {
        return schedule.ScheduleArrivals.reduce((accumulator, arrival) => ({
          ...accumulator,
          [arrival.RouteID]: (accumulator[arrival.RouteID] || []).concat(arrival.ScheduleTime),
        }), {});
      })
      .then((schedule) => {
        return Object.keys(schedule).map((route) => ({
          route,
          arrivals: schedule[route],
        }));
      });
  }
}
