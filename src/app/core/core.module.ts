import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { UserService } from './services/user.service';
import { AuthenticatedGuard } from './guards/authenticated.guard';
import { MapService } from './services/map.service';
import { WMATAService } from './services/wmata.service';

@NgModule({
  declarations: [],
  imports: [
    HttpClientModule,
    CommonModule,
  ],
  providers: [
    UserService,
    AuthenticatedGuard,
    MapService,
    WMATAService,
  ],
})
export class CoreModule {}
