import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { skipWhile, tap } from 'rxjs/operators';

import { UserService } from '../services/user.service';

@Injectable()
export class AuthenticatedGuard implements CanActivate {

  constructor(
    private router: Router,
    private userService: UserService,
  ) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.userService.isAuthenticated()
      .pipe(
        skipWhile((isUserAuthenticated: boolean | undefined) => isUserAuthenticated === undefined),
        tap((isUserAuthenticated: boolean) => {
          if (!isUserAuthenticated) {
            this.router.navigate([ '/login' ]);
          }
        }));
  }
}
