import { LatLngLiteral } from '@agm/core';

export class State {
  name: string;
  color: string;
  center: LatLngLiteral;
  coords: LatLngLiteral[];
}
