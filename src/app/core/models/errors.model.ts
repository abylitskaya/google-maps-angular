export class Errors {
  [errorName: string]: string;

  static toStringArray(errors: Errors): string[] {
    return Object.keys(errors).map((errorName) => {
      const title = errorName.split(' ').reduce((accumulator, word) => {
        return `${ accumulator } ${ word.charAt(0).toUpperCase() }${ word.slice(1) }`;
      }, '');
      return `${ title }: ${ errors[errorName] }`;
    });
  }
}
