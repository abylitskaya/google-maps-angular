import { BusStop } from './bus-stop.model';

export class ScheduleArrival {
  DirectionNum: string;
  EndTime: string;
  RouteID: string;
  ScheduleTime: string;
  StartTime: string;
  TripDirectionText: string;
  TripHeadsign: string;
  TripID: string;
}

export class BusStopSchedule {
  ScheduleArrivals: ScheduleArrival[];
  Stop: BusStop;
}
