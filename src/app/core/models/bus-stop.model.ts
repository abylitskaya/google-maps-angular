export class BusStop {
  Lat: number;
  Lon: number;
  Name: string;
  StopID: string;
  Routes: string[];
}

export class BusStopsParams {
  centerLat?: number;
  centerLng?: number;
  radius?: number;

  constructor(params: BusStopsParams = {}) {
    this.centerLat = params.centerLat || 38.8977035;
    this.centerLng = params.centerLng || -77.0366801;
    this.radius = params.radius || 1;
  }
}
