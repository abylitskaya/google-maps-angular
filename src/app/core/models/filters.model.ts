import { BusStopsParams } from './bus-stop.model';

export enum FiltersType {
  USA = 'states',
  Washington = 'busStops'
}

export class Filters {
  type: FiltersType;
  center: {
    lat: number,
    lng: number
  };
  zoom: number;

  states: boolean;
  statesInfo: boolean;

  radius: number;
  routes: string[];

  constructor(type: FiltersType = FiltersType.Washington) {
    this.type = type;
    this.center = type === FiltersType.USA
      ? { lat: 51.5087426124564, lng: -120.05859312500003 }
      : { lat: 38.8977035, lng: -77.0366801 };
    this.zoom = type === FiltersType.USA ? 4 : 14;
    this.states = false;
    this.statesInfo = false;
    this.radius = new BusStopsParams().radius;
    this.routes = [ 'all' ];
  }
}
