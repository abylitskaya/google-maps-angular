import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { GoogleMapsRoutingModule } from './app-routing.module';
import { LoginModule } from './login/login.module';
import { DashboardModule } from './dashboard/dashboard.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    LoginModule,
    DashboardModule,
    GoogleMapsRoutingModule,
  ],
  providers: [],
  bootstrap: [ AppComponent ],
})
export class AppModule {
}
