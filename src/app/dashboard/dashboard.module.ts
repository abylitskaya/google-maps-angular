import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AgmCoreModule } from '@agm/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MaterialModule } from '../shared/material.module';
import { CoreModule } from '../core/core.module';
import { environment } from '../../environments/environment';
import { FiltersComponent } from './filters/filters.component';
import { DashboardComponent } from './dashboard.component';
import { MapComponent } from './map/map.component';
import { FiltersService } from './core/services/filters.service';
import { BusStopInfoComponent } from './bus-stop-info/bus-stop-info.component';
import { BusStopInfoDialogComponent } from './bus-stop-info-dialog/bus-stop-info-dialog.component';

@NgModule({
  declarations: [
    DashboardComponent,
    FiltersComponent,
    MapComponent,
    BusStopInfoComponent,
    BusStopInfoDialogComponent,
  ],
  imports: [
    CommonModule,
    CoreModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    AgmCoreModule.forRoot(environment.amgConfig),
  ],
  providers: [
    FiltersService,
  ],
  entryComponents: [
    BusStopInfoDialogComponent,
  ],
})
export class DashboardModule {}
