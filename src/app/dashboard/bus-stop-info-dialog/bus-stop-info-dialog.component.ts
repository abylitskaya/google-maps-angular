import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-bus-stop-info-dialog',
  templateUrl: './bus-stop-info-dialog.component.html',
  styleUrls: [ './bus-stop-info-dialog.component.scss' ],
})
export class BusStopInfoDialogComponent {

  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {}

  trackBy(filed: string) {
    return (_, item) => {
      return item[filed];
    };
  }

}
