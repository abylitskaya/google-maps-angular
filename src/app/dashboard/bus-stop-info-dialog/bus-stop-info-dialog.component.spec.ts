import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusStopInfoDialogComponent } from './bus-stop-info-dialog.component';

describe('BusStopInfoDialogComponent', () => {
  let component: BusStopInfoDialogComponent;
  let fixture: ComponentFixture<BusStopInfoDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusStopInfoDialogComponent ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusStopInfoDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
