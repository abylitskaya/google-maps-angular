import { Component, Input } from '@angular/core';

import { BusStop } from '../../core/models/bus-stop.model';
import { WMATAService } from '../../core/services/wmata.service';
import { ScheduleArrival } from '../../core/models/bus-stop-schedule';
import { MatDialog } from '@angular/material';
import { BusStopInfoDialogComponent } from '../bus-stop-info-dialog/bus-stop-info-dialog.component';

@Component({
  selector: 'app-bus-stop-info',
  templateUrl: './bus-stop-info.component.html',
  styleUrls: [ './bus-stop-info.component.scss' ],
})
export class BusStopInfoComponent {
  @Input() busStop: BusStop;
  schedule: ScheduleArrival[];

  constructor(
    public dialog: MatDialog,
    private wmataService: WMATAService,
  ) {}

  openDialog(): void {
    this.dialog.open(BusStopInfoDialogComponent, {
      width: '600px',
      data: this.schedule,
    });
  }

  async getSchedule(): Promise<void> {
    this.schedule = await this.wmataService.getBusStopSchedule(this.busStop.StopID);
    this.openDialog();
  }
}
