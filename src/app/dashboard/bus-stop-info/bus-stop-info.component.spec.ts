import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusStopInfoComponent } from './bus-stop-info.component';

describe('BusStopInfoComponent', () => {
  let component: BusStopInfoComponent;
  let fixture: ComponentFixture<BusStopInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusStopInfoComponent ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusStopInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
