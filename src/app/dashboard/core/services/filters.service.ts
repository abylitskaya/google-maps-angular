import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

import { Filters } from '../../../core/models/filters.model';

@Injectable()
export class FiltersService {
  private filtersSubject = new BehaviorSubject<Filters>(new Filters());

  constructor() {}

  setFilters(filters: Filters): void {
    this.filtersSubject.next({
      ...new Filters(filters.type),
      ...filters,
    });
  }

  getFilters(): Observable<Filters> {
    return this.filtersSubject.asObservable();
  }
}
