import { Component, OnInit } from '@angular/core';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { LatLngLiteral } from '@agm/core';

import { MapService } from '../../core/services/map.service';
import { State } from '../../core/models/state.model';
import { FiltersService } from '../core/services/filters.service';
import { Filters, FiltersType } from '../../core/models/filters.model';
import { WMATAService } from '../../core/services/wmata.service';
import { BusStop } from '../../core/models/bus-stop.model';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: [ './map.component.scss' ],
})
export class MapComponent implements OnInit {
  type: FiltersType;

  zoom: number;
  lat: number;
  lng: number;

  showStates: boolean;
  showStatesInfo: boolean;

  states: State[] = [];
  radius: number;

  busStops: BusStop[] = [];
  visibleBusStops: BusStop[] = [];

  visibleInfobox: number | null;

  private onDestroy = new Subject<void>();

  constructor(
    private mapService: MapService,
    private filterService: FiltersService,
    private wmataService: WMATAService,
  ) {
    const { zoom, center } = new Filters(FiltersType.Washington);
    this.updateMap(zoom, center);
  }

  get showBusStops(): boolean {
    return this.type === FiltersType.Washington;
  }

  get showUSAStates(): boolean {
    return this.type === FiltersType.USA && this.showStates;
  }

  trackBy(filed: string) {
    return (_, item) => {
      return item[filed];
    };
  }

  onBusStopClick(index: number): void {
    this.visibleInfobox = index === this.visibleInfobox ? null : index;
  }

  async ngOnInit() {
    this.states = await this.mapService.getStates() || [];

    this.filterService.getFilters()
      .pipe(takeUntil(this.onDestroy))
      .subscribe((filters) => {
        this.handleFiltersChange(filters);
      });
  }

  private async handleFiltersChange(filters: Filters): Promise<void> {
    const { zoom, center } = filters;
    this.visibleInfobox = null;
    this.type = filters.type;

    this.updateMap(zoom, center);

    if (this.type === FiltersType.USA) {
      this.handleStatesParams(filters);
    } else {
      this.handleBusStopsParams(filters);
    }
  }

  private handleStatesParams(filters: Filters): void {
    const { states, statesInfo } = filters;
    this.showStates = states;
    this.showStatesInfo = statesInfo;
  }

  private async handleBusStopsParams(filters: Filters): Promise<void> {
    const { radius, routes } = filters;
    if (this.radius !== radius) {
      this.radius = radius;
      if (this.radius) {
        this.busStops = await this.wmataService.getBusStops({ radius });
      }
    }

    this.visibleBusStops = this.busStops.filter((stop) => {
      let visible = false;
      routes.forEach((route) => {
        if (`${ stop.Routes }`.includes(route) || route === 'all') {
          visible = true;
        }
      });
      return visible;
    });
  }

  private updateMap(zoom: number, center: LatLngLiteral): void {
    this.zoom = zoom;
    this.lat = center.lat;
    this.lng = center.lng;
  }
}
