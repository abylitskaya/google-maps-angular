import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { Filters, FiltersType } from '../core/models/filters.model';
import { FiltersService } from './core/services/filters.service';
import { UserService } from '../core/services/user.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: [ './dashboard.component.scss' ],
})
export class DashboardComponent {
  filtersState = 0;
  filtersOpened = true;
  filtersType: FiltersType = FiltersType.Washington;

  constructor(
    private userService: UserService,
    private filtersService: FiltersService,
    private router: Router,
  ) {}

  changeFiltersView(index: number): void {
    this.filtersType = {
      0: FiltersType.Washington,
      1: FiltersType.USA,
    }[index];
    this.filtersService.setFilters(new Filters(this.filtersType));
  }

  signOut(): void {
    this.userService.signOut();
    this.router.navigate([ '/login' ]);
  }
}
