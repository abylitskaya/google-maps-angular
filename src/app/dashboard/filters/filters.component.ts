import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatOption } from '@angular/material';

import { Filters, FiltersType } from '../../core/models/filters.model';
import { FiltersService } from '../core/services/filters.service';
import { WMATAService } from '../../core/services/wmata.service';
import { Route } from '../../core/models/route.model';

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: [ './filters.component.scss' ],
})
export class FiltersComponent implements OnInit {
  @Input() type: FiltersType;
  @ViewChild('allSelected') private allSelected: MatOption;
  filtersForm: FormGroup;

  routes: Route[] = [];

  constructor(
    private filtersService: FiltersService,
    private wmataService: WMATAService,
  ) {
    const { radius, routes, states, statesInfo } = new Filters();
    this.filtersForm = new FormGroup({
      radius: new FormControl(radius),
      routes: new FormControl(routes),
      states: new FormControl(states),
      statesInfo: new FormControl(statesInfo),
    });
  }

  get isWashington(): boolean {
    return this.type === FiltersType.Washington;
  }

  get isUSA(): boolean {
    return this.type === FiltersType.USA;
  }

  allRoutesChecked(): boolean {
    return this.filtersForm.get('routes').value.includes('all');
  }

  routeSelected(route: Route): boolean {
    return this.filtersForm.get('routes').value.includes(route.RouteID)
      || this.allRoutesChecked();
  }

  optionClick(): void {
    if (this.allSelected.selected) {
      this.allSelected.deselect();
    } else if (this.filtersForm.controls.routes.value.length === this.routes.length) {
      this.allSelected.select();
    }
  }

  toggleAllSelection() {
    if (this.allSelected.selected) {
      this.filtersForm.controls.routes
        .patchValue([ 'all' ]);
    } else {
      this.filtersForm.controls.routes.patchValue([]);
    }
  }

  submitFilters() {
    this.filtersService.setFilters({
      ...this.filtersForm.value,
      type: this.type,
    });
  }

  ngOnInit() {
    this.wmataService.getRoutes().then(routes => {
      this.routes = routes;
    });
  }

}
