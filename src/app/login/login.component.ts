import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { UserService } from '../core/services/user.service';
import { Errors } from '../core/models/errors.model';
import { fadeInOut } from '../core/animations';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: [ './login.component.scss' ],
  animations: [
    fadeInOut,
  ],
})
export class LoginComponent {
  loginForm: FormGroup;
  isLogin = true;
  errors: string[] = [];

  constructor(
    private router: Router,
    private userService: UserService,
  ) {
    this.loginForm = new FormGroup({
      email: new FormControl('', [ Validators.required, Validators.email ]),
      password: new FormControl('', [ Validators.required, Validators.minLength(6) ]),
    });
  }

  submitForm(): void {
    this.errors = [];
    if (this.loginForm.invalid) {
      return;
    }
    const action = this.isLogin ? 'checkLogin' : 'register';
    this.userService[action](this.loginForm.value)
      .then(
        this.showMap.bind(this),
        this.showErrors.bind(this),
      );
  }

  private showMap() {
    this.router.navigate([ '/dashboard' ]);
  }

  private showErrors(errors: Errors): void {
    this.errors = Errors.toStringArray(errors);
  }
}
